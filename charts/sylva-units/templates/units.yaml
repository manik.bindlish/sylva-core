{{- $envAll := set . "Values" (include "interpret-values-gotpl" . | fromJson) -}}
{{ range $unit_name, $unit_def := .Values.units }}

  {{- if include "unit-enabled" (tuple $envAll $unit_name) -}}
    {{/*********** Prepare labels used in all generated objects */}}

    {{- $labels := deepCopy ($unit_def.labels | default dict) -}}
    {{- $_ := set $labels "sylva-units.unit" $unit_name -}}
    {{- $_ := mergeOverwrite $labels (include "sylva-units.labels" $envAll | fromYaml) }}

    {{- $helmrelease_spec_overrides := dict -}}
    {{- $kustomization_spec_overrides := dict -}}

    {{- if not (hasKey $unit_def "helm_repo_url") -}}
      {{/* if no helm_repo_url is set, then we know we'll use a GitRepository or OCIRepository */}}
      {{/* the GitRepository or OCIRepository used here are generated in 'sources.yaml' */}}

      {{- $repo_name := $unit_def.repo -}}
      {{- $repo_def := index $envAll.Values.source_templates $repo_name -}}{{/* ??? */}}

      {{- $sourceRef := dict -}}
      {{- if get $repo_def "existing_source" -}}
        {{- $_ = set $sourceRef "name" $repo_def.existing_source.name -}}
        {{- $_ = set $sourceRef "kind" $repo_def.existing_source.kind -}}
      {{- else -}}
        {{- $_ = set $sourceRef "name" $repo_name -}}
        {{- $_ = set $sourceRef "kind" $repo_def.kind -}}
      {{- end -}}

      {{ if hasKey $unit_def "helmrelease_spec" -}}
        {{ if eq $repo_def.kind "OCIRepository" }}
          {{/* if the source repo is not of type GitRepository then we fail as we don't know how to do that yet */}}
          {{ fail (printf "You cannot use helm_release_spec for a unit using 'repo' and referencing a source repo of type OCIRepository (unit '%s')" $unit_name) }}
        {{ end }}
        {{/*
        if helm_release_spec is used and the source repo is of type GitRepository then we inject our GitRepository in it as sourceRef
        (the Kustomization will in that case, see in 'unit_helmrelease_kustomization_spec_default', use 'sylva-core' as the GitRepository)
        */}}
        {{- $helmrelease_spec_overrides = dict "chart" (dict "spec" (dict "sourceRef" $sourceRef
                                                                          "reconcileStrategy" "Revision")) -}}
      {{- else -}}{{/* if 'helmrelease_spec' isn't used, then the source from 'repo' is used for the Kustomization */}}
        {{- $kustomization_spec_overrides = dict "sourceRef" $sourceRef -}}
      {{- end }}

    {{ else }}{{/* helm_repo is declared, we generate a HelmRepository */}}

      {{- $helm_repo_spec := dict -}}
      {{- $schema := urlParse $unit_def.helm_repo_url -}}
        {{- if $schema.scheme | eq "oci" -}}
          {{- $helm_repo_spec = dict "type" "oci" -}}
        {{- end -}}
      {{- $helm_repo_spec = mergeOverwrite $helm_repo_spec $envAll.Values.helm_repo_spec_default (dict "url" $unit_def.helm_repo_url) -}}

      {{/*
      we inject a sourceRef pointing to our dynamically created HelmRepository
      (unless the unit definition provides a chart.spec.sourceRef, which we do in the
      specific case of bootstrap/mgmt handover, see bootstrap.values.yaml 'sylva-units' unit)
      */}}
      {{- if not ($unit_def.helmrelease_spec | dig "chart" "spec" "sourceRef" dict) -}}
        {{- $helmrelease_spec_overrides = dict "chart" (dict "spec" (dict "sourceRef" (dict "kind" "HelmRepository" "name" (printf "unit-%s" $unit_name)))) -}}
      {{- end }}
---
apiVersion: source.toolkit.fluxcd.io/v1beta2
kind: HelmRepository
metadata:
  name: unit-{{ $unit_name }}
  labels:
{{ $labels | toYaml | indent 4 }}
spec: {{ $helm_repo_spec | toYaml | nindent 2 }}
    {{ end -}}

    {{/******************* Generation of the Kustomization */}}

    {{- $patches := list -}}

    {{/* here, if "helmrelease_spec" is used, we define the spec of the inner HelmRelease */}}
    {{- if hasKey $unit_def "helmrelease_spec" -}}
      {{/* this is where we refer to ./kustomize-units/helmrelease-generic */}}
      {{- $kustomization_spec_overrides = mergeOverwrite $kustomization_spec_overrides $envAll.Values.unit_helmrelease_kustomization_spec_default -}}

      {{/* combine '_postRenderers' with 'postRenderers' */}}
      {{- $post_renderers := concat ($unit_def.helmrelease_spec | dig "_postRenderers" list)
                                    ($unit_def.helmrelease_spec | dig "postRenderers" list) -}}

      {{/* here we workaround the fact that 'concat', when only given empty lists,
      returns some internal go object that does not render as "[]", but as "null"
      */}}
      {{- if not $post_renderers -}}
        {{- $post_renderers = list -}} {{/* reset to a simple empty list */}}
      {{- end -}}

      {{/* ensure that we clean _postRenderers from the rendered resources */}}
      {{- $_ := unset $unit_def.helmrelease_spec "_postRenderers" -}}

      {{/* produce the HelmRelease specification, merging the different sources we have */}}
      {{- $helmrelease_spec := dict "releaseName" $unit_name -}}
      {{- $helmrelease_spec := mergeOverwrite $helmrelease_spec $envAll.Values.unit_helmrelease_spec_default $unit_def.helmrelease_spec $helmrelease_spec_overrides (dict "postRenderers" $post_renderers) -}}

      {{- if hasKey $unit_def "helm_secret_values" -}}
        {{/* if the unit has "helm_secret_values" defined, then
             we add to valuesFrom a Secret with "helm_secret_values"
        */}}
        {{- $secretName := printf "helm-unit-values-%s" $unit_name }}
---
apiVersion: v1
kind: Secret
metadata:
  name: {{ $secretName }}
  labels: {{ $labels | toYaml | nindent 4 }}
stringData:
  values: |{{ $unit_def.helm_secret_values | toYaml | nindent 4 }}

        {{ $valuesFrom := $helmrelease_spec.valuesFrom | default list -}}
        {{- $secretsValues := dict "kind" "Secret" "name" $secretName "valuesKey" "values" -}}

        {{- $_ := set $helmrelease_spec "valuesFrom" (append $valuesFrom $secretsValues) -}}

      {{- end -}}

      {{/* inject the HelmRelease spec via a patch in the Kustomization: */}}
      {{- $patch := include "helmrelease-kustomization-patch-template" (tuple $unit_name $helmrelease_spec $labels) | fromYaml -}}
      {{- $patches = list $patch -}}
    {{- end -}}

    {{- $kustomization_spec := dict -}}

    {{- if hasKey $unit_def "kustomization_substitute_secrets" -}}
        {{/* if the unit has "kustomization_substitute_secrets" defined, then
             we add to substituteFrom a Secret with "kustomization_substitute_secrets"
        */}}
        {{- $secretName := printf "kustomization-unit-substitute-%s" $unit_name }}
---
apiVersion: v1
kind: Secret
metadata:
  name: {{ $secretName }}
  labels: {{ $labels | toYaml | nindent 4 }}
stringData:
{{- $unit_def.kustomization_substitute_secrets | toYaml | nindent 2 }}

      {{- $substituteFrom := ($unit_def.kustomization_spec.postBuild | default dict).substituteFrom | default list -}}
      {{- $secretsValues := dict "kind" "Secret" "name" $secretName -}}

      {{- $_ := mergeOverwrite $kustomization_spec_overrides (dict "postBuild" (dict "substituteFrom" (append $substituteFrom $secretsValues))) -}}
    {{- end -}}

    {{/* let's set the dependsOn field */}}
    {{- $dependsOn := dict -}}
    {{- if hasKey $unit_def "depends_on" -}}
      {{- $dependsOnList := list -}}
      {{- range $dep_unit_name, $is_depend_on := $unit_def.depends_on -}}
        {{- if tuple $envAll $is_depend_on (printf "%s, depends on %s" $unit_name $dep_unit_name) | include "interpret-for-test" -}}

          {{- if not (hasKey $envAll.Values.units $dep_unit_name) -}}
            {{- fail (printf "unit '%s' is declared with a dependency on non-existing unit '%s'" $unit_name $dep_unit_name) -}}
          {{- else if not (include "unit-enabled" (tuple $envAll $dep_unit_name)) -}}
            {{- fail (printf "unit '%s' is declared with a dependency on disabled unit '%s'" $unit_name $dep_unit_name) -}}
          {{- end -}}

          {{- $dependsOnList = append $dependsOnList (dict "name" $dep_unit_name) -}}  

        {{- end -}}
      {{ end }}
      {{- $_ := set $dependsOn "dependsOn" $dependsOnList -}}
    {{- end -}}

    {{/* combine '_patches' with 'patches' and the $patches computed earlier in this template */}}
    {{- $patches := concat $patches ($unit_def | dig "kustomization_spec" "_patches" list)
                                    ($unit_def | dig "kustomization_spec" "patches" list) -}}
    {{/* combine '_components' with 'components' */}}
    {{- $components := concat ($unit_def | dig "kustomization_spec" "_components" list)
                              ($unit_def | dig "kustomization_spec" "components" list) -}}

    {{/* ensure that we clean _patches and _components from the rendered resources */}}
    {{- if hasKey $unit_def "kustomization_spec" -}}
      {{- $_ := unset $unit_def.kustomization_spec "_patches" -}}
      {{- $_ := unset $unit_def.kustomization_spec "_components" -}}
    {{- end -}}

    {{/* here we workaround the fact that 'concat', when only given empty lists,
     returns some internal go object that does not render as "[]", but as "null"
    */}}
    {{- if not $patches -}}
      {{- $patches = list -}} {{/* reset to a simple empty list */}}
    {{- end -}}
    {{- if not $components -}}
      {{- $components = list -}} {{/* reset to a simple empty list */}}
    {{- end -}}

    {{/* finalize the definition of the Kustomization spec by merging everything we have */}}
    {{- $kustomization_spec = mergeOverwrite $kustomization_spec $envAll.Values.unit_kustomization_spec_default ($unit_def.kustomization_spec | default dict) $dependsOn $kustomization_spec_overrides (dict "patches" $patches) (dict "components" $components) -}}

    {{/* 
    if healthChecks is specified on kustomization_spec while helmrelease_spec is defined,
    then we inject a reference to the HelmRelease in the healthChecks, to ensure
    that the HelmRelease object will not be omitted
    */}}
    {{- if and (hasKey $kustomization_spec "healthChecks")
               (hasKey $unit_def "helmrelease_spec") -}}
      {{- $_ := set $kustomization_spec "healthChecks" (append $kustomization_spec.healthChecks (dict "namespace" $envAll.Release.Namespace "apiVersion" "helm.toolkit.fluxcd.io/v2beta1" "kind" "HelmRelease" "name" $unit_name)) -}}
      {{- $_ := set $kustomization_spec "wait" false -}}
    {{- end -}}

    {{- if and (gt (len (get $kustomization_spec "healthChecks" | default dict)) 0) (get $kustomization_spec "wait" | default false) -}}
      {{- fail (printf "unit '%s' has healthChecks defined but 'wait' set to true, which is inconsistent because healthChecks are ignored with 'wait: true')" $unit_name) -}}
    {{- end }}
---
apiVersion: kustomize.toolkit.fluxcd.io/v1beta2
kind: Kustomization
metadata:
  name: {{ $unit_name }}
  labels: {{ $labels | toYaml | nindent 4 }}
  annotations: {{ $unit_def.annotations | default dict | toYaml | nindent 4 }}
spec: {{ $kustomization_spec | toYaml | nindent 2 }}
  {{- end -}}
{{ end }}
